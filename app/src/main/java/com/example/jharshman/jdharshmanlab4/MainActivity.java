package com.example.jharshman.jdharshmanlab4;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private SevenSegment mSevenSegment;
    private SevenSegment mSevenSegment2;
    private SevenSegment mSevenSegment3;
    private SevenSegment mSevenSegment4;
    private int segmentCount = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSevenSegment = (SevenSegment)findViewById(R.id.view);
        mSevenSegment2 = (SevenSegment)findViewById(R.id.view2);
        mSevenSegment3 = (SevenSegment)findViewById(R.id.view3);
        mSevenSegment4 = (SevenSegment)findViewById(R.id.view4);

        //Floating action button spawns alternative "about me"
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aboutMe();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void aboutMe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.attr_dialog_title)
                .setItems(R.array.about_me, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        builder.create();
        builder.show();
    }

    public void incrementSegment(View v) {
        segmentCount++;
        if(segmentCount == 10) {
            //turn off display and set count back to zero
            mSevenSegment.setCurrent(segmentCount);
            mSevenSegment2.setCurrent(segmentCount+1);
            mSevenSegment3.setCurrent(segmentCount+2);
            mSevenSegment4.setCurrent(segmentCount+3);
            segmentCount = -1;
        } else {
            //set to display whatever number we are at
            mSevenSegment.setCurrent(segmentCount);
            mSevenSegment2.setCurrent(segmentCount+1);
            mSevenSegment3.setCurrent(segmentCount+2);
            mSevenSegment4.setCurrent(segmentCount+3);
        }

        //this will trigger the redraw
        mSevenSegment.invalidate();
        mSevenSegment2.invalidate();
        mSevenSegment3.invalidate();
        mSevenSegment4.invalidate();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("Count", segmentCount);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        segmentCount = savedInstanceState.getInt("Count");
    }
}
