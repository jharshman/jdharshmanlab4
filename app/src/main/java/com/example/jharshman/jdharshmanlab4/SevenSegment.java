package com.example.jharshman.jdharshmanlab4;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by jharshman on 10/27/15.
 */
public class SevenSegment extends View {

    final int WIDTH         = 12;
    final int HEIGHT        = 18;
    final int MARGIN        = 2;
    final float ASPECTRATIO = (float)(WIDTH)/(float)(HEIGHT);
    final int LISTSIZE      = 6;
    final int RED           = Color.rgb(255, 0, 0);
    final int DRED          = Color.rgb(76,0,0);

    final boolean lookupTable[][] = {
            {true, true, true, true, true, true, false},        // zero
            {true, true, false, false, false, false, false},    // one
            {true, false, true, true, false, true, true},       // two
            {false, false, true, true, true, true, true},       // three
            {false, true, false, true, true, false,true},       // four
            {false, true, true, false, true, true, true},       // five
            {true, true, true, false, true, true, true},        // six
            {false, false, true, true, true, false, false},     // seven
            {true, true, true, true, true, true, true},         // eight
            {false, true, true, true, true, false, true},       // nine
            {false, false, false, false, false, false, false}   // ten
    };

    private int mCurrent; //initial state of display is off
    private Path mPath;
    private ArrayList<Boolean> mSegStatus = new ArrayList<Boolean>(LISTSIZE);
    private final float coordinates[] = {3,9,4,10,4,14,3,15,2,14,2,10};
    private int mIndex;
    private float mScaledWidth;
    private float mScaledHeight;

    public void setCurrent(int pCurrent) {
        if(pCurrent >= 0 && pCurrent <= 10) {
            mCurrent = pCurrent;
            setSegStatus(lookupTable[pCurrent]);
        } else {
            setSegStatus(lookupTable[10]); // shitty input will just keep the display off
        }
    }

    public void setSegStatus(boolean table[]) {
        if(!(mSegStatus.isEmpty()))
            mSegStatus.clear();
        for(boolean b : table) {
            mSegStatus.add(b);
        }
    }

    public SevenSegment(Context context) {
        super(context);
    }

    public SevenSegment(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SevenSegment(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPath = new Path();
        float scaleWidth = mScaledWidth/(float)(WIDTH);
        float scaleHeight = mScaledHeight/(float)(HEIGHT);

        setLayerType(this.LAYER_TYPE_SOFTWARE, null);

        //there is nothing elegant about this
        canvas.scale(scaleWidth, scaleHeight);
        canvas.save();
        makePath(canvas);
        mIndex++;
        canvas.restore();
        canvas.save();
        canvas.translate(0, -6);
        makePath(canvas);
        mIndex++;
        canvas.restore();
        canvas.save();
        canvas.translate(0, -6);
        canvas.rotate(-90, 3, 9);
        makePath(canvas);
        mIndex++;
        canvas.restore();
        canvas.save();
        canvas.translate(0, -6);
        canvas.translate(6, 0);
        makePath(canvas);
        mIndex++;
        canvas.restore();
        canvas.save();
        canvas.translate(6, 0);
        makePath(canvas);
        mIndex++;
        canvas.restore();
        canvas.save();
        canvas.rotate(90, 3, 15);
        makePath(canvas);
        mIndex++;
        canvas.restore();
        canvas.save();
        canvas.rotate(-90, 3, 9);
        makePath(canvas);
        mIndex++;
        canvas.restore();
        canvas.save();
        mIndex=0;

    }

    private void makePath(Canvas canvas) {

        mPath.moveTo(coordinates[0], coordinates[1]);
        for( int i = 2; i < coordinates.length; i=i+2 ) {
            mPath.lineTo(coordinates[i], coordinates[i+1]);
        }
        mPath.close();
        canvas.clipPath(mPath);
        if(mSegStatus.isEmpty()) {
            canvas.drawColor(DRED);
        }
        else {
            if(mSegStatus.get(mIndex) == true)
                canvas.drawColor(RED);
            else
                canvas.drawColor(DRED);
        }
    }

    //not really needed
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mScaledWidth=w;
        mScaledHeight=h;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = (int)(MeasureSpec.getSize(heightMeasureSpec) * ASPECTRATIO);
        int height = (int)(MeasureSpec.getSize(widthMeasureSpec) / ASPECTRATIO);

        if(height > MeasureSpec.getSize(heightMeasureSpec)) height = MeasureSpec.getSize(heightMeasureSpec);
        else                                                width = MeasureSpec.getSize(widthMeasureSpec);

        setMeasuredDimension(width, height);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("InstanceState", super.onSaveInstanceState());
        bundle.putInt("Index", mIndex);
        bundle.putInt("Current", mCurrent);
        bundle.putSerializable("SegmentArray", mSegStatus);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if(state instanceof Bundle) {
            Bundle bundle = (Bundle)state;
            mIndex = bundle.getInt("Index");
            mCurrent = bundle.getInt("Current");
            mSegStatus = (ArrayList<Boolean>)bundle.getSerializable("SegmentArray");
            state = bundle.getParcelable("InstanceState");
        }
        super.onRestoreInstanceState(state);
    }
}
